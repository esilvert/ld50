extends Movable

onready var fsm = $fsm
onready var shape = $shape
onready var shader_material = $shader.material
onready var possess_area = $possess_area
onready var heaven = $heaven
onready var sound_player = $sounds

var possess_target_ref = null
var current_dimension = null

func _ready():
	fsm.STATES.append_array(['idle', 'moving', 'haunting'])

	possess_area.connect('npc_entered', self, 'on_npc_entered')
	possess_area.connect('npc_exited', self, 'on_npc_exited')

	g_dimension.connect('dimension_changed', self, 'on_dimension_changed')

func _exit_tree():
	g_dimension.disconnect('dimension_changed', self, 'on_dimension_changed')

func before_action(delta):
	handle_movement_inputs(delta)

func after_action(delta):
	move_with_velocity(false)

# --------------------------------------------------
# STATES
# --------------------------------------------------

func state_spawn(delta):
	fsm.set_next_state('idle')

func state_idle(delta):
	if (movement_allowed and velocity != Vector2(0,0)):
		fsm.set_next_state('moving')

	if (Input.is_action_just_pressed('player_possess') and possess_target()):
		possess()

	if (Input.is_action_just_pressed('player_switch_dimension')):
		g_dimension.toggle_hell()
		sound_player.play_sound('dimension_change')

func state_moving(delta):
	if (velocity == Vector2(0,0)):
		fsm.set_next_state('idle')
	if (Input.is_action_just_pressed('player_possess') and possess_target()):
		possess()
	if (Input.is_action_just_pressed('player_switch_dimension')):
		g_dimension.toggle_hell()
		sound_player.play_sound('dimension_change')

func state_haunting(delta):
	global_position = possess_target().global_position

	if(Input.is_action_just_pressed('player_possess') and possess_target()):
		unpossess()
		fsm.set_next_state('idle')

# --------------------------------------------------
# CALLBACKS
# --------------------------------------------------

func on_enter_spawn():
	movement_allowed = false

func on_enter_idle():
	movement_allowed = true

func on_enter_moving():
	movement_allowed = true

func on_enter_haunting():
	sound_player.play_sound('possess')
	movement_allowed = false
	possess_target().movement_allowed = true
	g_dimension.set_dimension('life')
	disable_collisions()
	possess_target().set_possessed(self)

func on_exit_haunting():
	sound_player.play_sound('planne_chelou')
	g_dimension.set_dimension('heaven')

func on_npc_entered(npc):
	if (fsm.current_state == 'haunting'):
		return

	prints('Player remembers the npc to possess')
	possess_target_ref = weakref(npc)

func on_npc_exited(npc):
	if (fsm.current_state == 'haunting'):
		return

	if (possess_target() == npc && !fsm.next_state): # crashed when lost in between two state
		prints('Player forgets the npc to possess')
		possess_target_ref = null

func on_dimension_changed(dimension):
	current_dimension = dimension
	if dimension == 'life':
		shader_material.set_shader_param('grayscale', false)
		heaven.hide()
	elif dimension == 'heaven':
		shader_material.set_shader_param('grayscale', true)
		heaven.show()
	else:
		pass

# --------------------------------------------------
# Actions
# --------------------------------------------------
func possess():
	if (!possess_target()):
		push_error('trying to possess without possess_target')
		return

	fsm.set_next_state('haunting')

func unpossess():
	if (!possess_target()):
		push_error('trying to unpossess without possess_target')
		return

	possess_target().set_unpossessed()
	fsm.set_next_state('idle')

func possess_target():
	if(!possess_target_ref):
		return null

	return possess_target_ref.get_ref()


func disable_collisions():
	shape.disabled = true

func enable_collisions():
	shape.disabled = false

func handle_movement_inputs(delta):
	velocity *= 0.99
	if Input.is_action_pressed('player_left'):
		velocity.x -= SPEED * delta
	if Input.is_action_pressed('player_right'):
		velocity.x += SPEED * delta

	if Input.is_action_pressed('player_up'):
		velocity.y -= SPEED * delta
	if Input.is_action_pressed('player_down'):
		velocity.y += SPEED * delta
