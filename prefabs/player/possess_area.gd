extends Area2D

signal npc_entered(npc)
signal npc_exited(npc)

func _ready():
	connect('body_entered', self, 'body_entered')
	connect('body_exited', self, 'body_exited')

func body_entered(body):
	if(body.is_in_group('npcs')):
		prints('PossessArea detected an npc', body.name)
		emit_signal('npc_entered', body)


func body_exited(body):
	if(body.is_in_group('npcs')):
		prints('PossessArea lost an npc', body.name)
		emit_signal('npc_exited', body)
