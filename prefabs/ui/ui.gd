extends Control

onready var progress_bar = $progress
onready var progress_bar_label = $progress/remaining_time/count
onready var remaining_time = $progress/remaining_time/count
onready var nb_deaths = $nb_deaths/count
onready var nb_saved = $nb_saved/count

func _ready():
	progress_bar.value = 0
	progress_bar_label.text = '0:00'
	remaining_time.text = '0'
	nb_deaths.text = '0'
	nb_saved.text = '0'

	set_process(true)

func _process(delta):

	var elapsed_time = g_win_lose_conditions.elapsed_time
	var remaining_time = g_win_lose_conditions.WIN_DURATION - elapsed_time
	progress_bar.value = elapsed_time

	progress_bar_label.text = "%02d:%02d" %[int(remaining_time / 60), int(remaining_time) % 60]

	nb_deaths.text = String(g_win_lose_conditions.nb_death_recorded)
	nb_saved.text = String(g_win_lose_conditions.nb_souls_saved_recorded)

	var ratio = float(g_win_lose_conditions.nb_souls_saved_recorded) - g_win_lose_conditions.nb_death_recorded
	if ratio > 0:
		$will_fire/count.text = 'Yes'
	else:
		$will_fire/count.text = 'No'
