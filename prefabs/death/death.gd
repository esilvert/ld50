extends Navigable

const SWITCH_TARGET_TRESHOLD = 100

onready var fsm = $fsm
onready var area = $area
onready var sound_player = $sounds

var targets = []
var target = null

const other_visual = preload('res://prefabs/death/death_0.png')


func _ready():
	if(int(rand_range(0, 10)) % 2 == 0):
		$sprite.texture = other_visual
	fsm.STATES.append_array(['hunt'])
	area.connect('npc_entered', self, "on_npc_entered")

	g_dimension.connect('dimension_changed', self, 'on_dimension_changed')
	on_dimension_changed(g_dimension.current_dimension)

	targets = navigation.get_children()

func _exit_tree():
	g_dimension.disconnect('dimension_changed', self, 'on_dimension_changed')

func before_action(delta):
	if(!target or !target.get_ref()):
		return

	if (path.empty()):
		pick_new_path()
	else:
		move_to_next_point_without_collision(delta)

func after_action(delta):
	move_with_velocity()

# --------------------------------------------------
# STATES
# --------------------------------------------------

func state_spawn(delta):
	var npcs = get_tree().get_nodes_in_group('npcs')

	if npcs.empty():
		return

	fsm.set_next_state('hunt')

func state_hunt(delta):
	if(!target or !target.get_ref()):
		pick_new_target()
		return

	if (target.get_ref().has_method('is_dead') and target.get_ref().is_dead()):

		velocity = Vector2(0, 0)

	if(fsm.state_duration > SWITCH_TARGET_TRESHOLD):
		fsm.state_duration = 0
		pick_new_target()



# --------------------------------------------------
# CALLBACKS
# --------------------------------------------------
func on_enter_hunt():
	pick_new_target()

func on_npc_entered(npc):
	target = weakref(npc)
	refresh_path()
	npc.die()

func on_dimension_changed(dimension):
	if dimension == 'hell':
		show()
	else:
		hide()

# --------------------------------------------------
# ACTIONS
# --------------------------------------------------

func pick_new_target():
	var random_index = rand_range(0, targets.size())
	target = weakref(targets[random_index])

	if target.get_ref():
		prints('Death ', name, 'picked a new target:', target.get_ref().name)

func pick_new_path():
	pick_new_target()

	if (!target.get_ref()):
		return

	refresh_path()


func refresh_path():
	path = navigation.get_simple_path(self.global_position, target.get_ref().global_position, true)
