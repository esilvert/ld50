extends Area2D

signal npc_entered(npc)

func _ready():
	connect('body_entered', self, "on_body_entered")


func on_body_entered(body):
	if(body.is_in_group('npcs')):
		emit_signal('npc_entered', body)
