extends AudioStreamPlayer2D

const SOUNDS = {
	'danger' : preload('res://assets/sounds/danger.wav'),#
	'danger_2' : preload('res://assets/sounds/danger_2.wav'),#
	'deep_brr' : preload('res://assets/sounds/deep_brrr.wav'),
	'dimension_change' : preload('res://assets/sounds/dimension_change.wav'), # ok
	'miaou' : preload('res://assets/sounds/miaou.wav'),
	'ouuuh' : preload('res://assets/sounds/ouuuh.wav'),
	'planne_chelou' : preload('res://assets/sounds/planne_chelou.wav'),
	'possess' : preload('res://assets/sounds/possess.wav'), # ok
	'saved' : preload('res://assets/sounds/saved_3.wav'), #
	'spawn' : preload('res://assets/sounds/spawn.wav'), #
	'spawn_enemy' : preload('res://assets/sounds/spawn_enemy.wav'), #
	'spawn_enemy_2' : preload('res://assets/sounds/spawn_enemy_2.wav'), #
	}


func play_sound(id):
	assert(SOUNDS.keys().find(id) != -1, "Invalid sound id : %s" % id)
	stream = SOUNDS[id]
	play()
