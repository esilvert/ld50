extends Navigable
class_name NPC

onready var fsm = $fsm

const DEATH_DURATION = 5 #s
const UNSPAWN_DURATION = 0.5 #s

const DEATH_SCENE = preload('res://prefabs/death/death.tscn')

onready var sound_player = $sounds

var haunter : Node2D = null

func _ready():
	SPEED = 80
	fsm.STATES.append_array(['idle', 'wander', 'possessed', 'dying', 'unspawn'])
	change_path_after = 3
	g_dimension.connect('dimension_changed', self, 'on_dimension_changed')

func _exit_tree():
	g_dimension.disconnect('dimension_changed', self, 'on_dimension_changed')

func after_action(delta):
	move_with_velocity()


# --------------------------------------------------
# STATES
# --------------------------------------------------

func state_idle(delta):
	if (rand_range(0, fsm.state_duration) > 3):
		fsm.set_next_state('wander')

func state_spawn(delta):
	fsm.set_next_state('wander')

func state_wander(delta):
	if(path.empty()):
		fsm.set_next_state('idle')
		return

	move_to_next_point(delta)

func state_possessed(delta):
	velocity = Vector2(0, 0)

	if Input.is_action_pressed('player_left'):
		velocity.x -= 1
	if Input.is_action_pressed('player_right'):
		velocity.x += 1

	if Input.is_action_pressed('player_up'):
		velocity.y -= 1
	if Input.is_action_pressed('player_down'):
		velocity.y += 1

func state_dying(delta):
	if(fsm.state_duration > DEATH_DURATION):
		g_win_lose_conditions.register_npc_death()
		var new_death = DEATH_SCENE.instance()

		get_parent().add_child(new_death)
		new_death.global_position = global_position
		queue_free()

func state_unspawn(delta):
	if(fsm.state_duration > UNSPAWN_DURATION):
		g_win_lose_conditions.register_npc_saved()
		queue_free()

# --------------------------------------------------
# CALLBACKS
# --------------------------------------------------
func on_enter_spawn():
	sound_player.play_sound('spawn')

func on_enter_idle():
	velocity = Vector2(0, 0)

func on_enter_wander():
	velocity = Vector2(0, 0)
	pick_new_path()
	prints("NPC will move to: ", path)

func on_enter_dying():
	if randf() > 0.5:
		sound_player.play_sound('danger')
	else :
		sound_player.play_sound('danger_2')

	prints("NPC %s is dead" % name)
	movement_allowed = false
	velocity = Vector2(0, 0)

func on_exit_dying():
	sound_player.play_sound('saved')

	movement_allowed = true

func on_enter_unspawn():
	prints("NPC %s is saved" % name)
	movement_allowed = false
	velocity = Vector2(0, 0)

func on_dimension_changed(dimension):
	if dimension == 'hell':
		hide()
	else:
		show()
# --------------------------------------------------
# Actions
# --------------------------------------------------

func set_possessed(player):
	haunter = player
	fsm.set_next_state('possessed')

func set_unpossessed():
	haunter = null
	fsm.set_next_state('idle')

func die():
	if(haunter):
		haunter.unpossess()

	fsm.set_next_state('dying')

func unspawn():
	if(haunter):
		haunter.unpossess()
	fsm.set_next_state('unspawn')

func is_dead():
	fsm.current_state == 'dying'
