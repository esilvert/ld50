extends Navigation2D

var POINTS_OF_INTERESTS = []

func _ready():
	POINTS_OF_INTERESTS = get_children()

	assert(POINTS_OF_INTERESTS.empty() == false, "Navigation must have points of interest")

func get_simple_path_to_point_of_interest(from):
	var random_index = rand_range(0, POINTS_OF_INTERESTS.size())
	var random_point = POINTS_OF_INTERESTS[random_index]

	return get_simple_path(from, random_point.global_position, true)
