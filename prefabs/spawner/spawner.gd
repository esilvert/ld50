extends Node2D

export(PackedScene) var ENTITY = null
export(NodePath) var SPAWN_POINT = null
export(int, 1, 1000) var SPAWN_SLEEPING_DURATION = 60
export(int, 1, 1000) var SPAWN_SLEEPING_OFFSET = 20

onready var fsm = $fsm

func _ready():
	assert(ENTITY, "Spawner must have an entity to spawn")
	assert(SPAWN_POINT, "Spawner must have a spawn point")
	assert(SPAWN_SLEEPING_DURATION, "Spawner must have a valid spawn frequency")
	assert(fsm, "Spawner must have an fsm")

	fsm.STATES = ['spawn', 'sleep', 'do_spawn']
	set_physics_process(true)

# --------------------------------------------------
# STATES
# --------------------------------------------------
func state_sleep(delta):
	$progress.value = fsm.state_duration

	if fsm.state_duration > SPAWN_SLEEPING_DURATION:
		fsm.set_next_state('do_spawn')


# --------------------------------------------------
# STATES
# --------------------------------------------------
func on_enter_spawn():
	$progress.value = 0
	$progress.max_value = SPAWN_SLEEPING_DURATION
	fsm.set_next_state('sleep')

func on_enter_do_spawn():
	spawn()
	fsm.set_next_state('sleep')


func on_enter_sleep():
	if fsm.previous_state != 'do_spawn':
		fsm.state_duration = SPAWN_SLEEPING_OFFSET


# --------------------------------------------------
# Actions
# --------------------------------------------------

func spawn():
	var new_child = ENTITY.instance()
	add_child(new_child)
	new_child.global_position = get_node(SPAWN_POINT).global_position
