extends AnimationPlayer

const GAME_SCENE = preload('res://levels/level_0/game.tscn')

var step = 0
var max_step = 0
var player = null
onready var text = $layer/panel/text
var area_positions = []
var current_area_position = null

func _ready():
	step = 0
	max_step = 0

	area_positions = $area_positions.get_children()
	current_area_position = null

	$npc.movement_allowed = false

	$death.fsm.set_physics_process(false)

	g_dimension.set_dimension('life')
	g_dimension.connect('dimension_changed', self, 'on_dimension_changed')

func _exit_tree():
	g_dimension.disconnect('dimension_changed', self, 'on_dimension_changed')

func next_step():
	step += 1
	max_step = max(step, max_step)

	call("step_%d" % step)

func step_1():
	player.get_node('living_body').queue_free()
	g_dimension.set_dimension('heaven')
	$layer/panel.show()
	player.movement_allowed = false

func step_2():
	text.text = "Yes. I know. Hard to believe uh ?"

func step_3():
	text.text = "In fact, I killed you... I need you."

func step_4():
	text.text = "You know, as a guardian angel you don't have a lot of time"

func step_5():
	text.text = "Sooo.. I killed you so you can help me ! Become a Guardian Angel ! Yay !"

func step_6():
	text.text = "I'm going to teach you reeeal quick, then you'll replace me the time I can smoke one"

func step_7():
	text.text = "Here, I'll show you, come here."

func step_8():
	if max_step == 8:
		move_area()
	player.movement_allowed = true
	$layer/panel.hide()

func step_9():
	player.movement_allowed = false
	$layer/panel.show()
	text.text = "This is a living. This is someone like you a minute ago"

func step_10():
	text.text = "Livings leaves peacefully, but sometime they die. Do you know why ?"

func step_11():
	text.text = "I'll show you, but first, you need to learn a super power."

func step_12():
	text.text = "When close to this living, you can embody them. Try it with this one, I think their name is Jimmy... Or maybe Alba..."

func step_13():
	text.text = "Once you're in, meet me further east !"

func step_14():
	if max_step == 14:
		move_area()
	player.movement_allowed = true
	$layer/panel.hide()
	# player.get_node('tuto_shape').disabled = true

func step_15():
	prints('player state', player.fsm.current_state)
	if player.fsm.current_state != 'haunting':
		step = 8
		next_step()
		player.global_position = $npc.global_position - Vector2(100, 0)
		return

	player.movement_allowed = false
	$npc.movement_allowed = false
	$layer/panel.show()
	text.text = "As you are technically in the living dimension, you can see everything with their eyes. This building is like their home. Jimmy lives here ! They live here, reproduce and sometime leave it to adventure in the wild."

func step_16():
	text.text = "So try to remember the location of these things, it will help you predict were the living will be. Now go a little bit further."

func step_17():
	player.movement_allowed = true
	$npc.movement_allowed = true
	$layer/panel.hide()
	move_area()

func step_18():
	$npc.movement_allowed = false
	player.movement_allowed = false
	$layer/panel.show()
	text.text = "Okay. This is a safe place. We call this, an \"Unspawner\". Our creator lacks originality."

func step_19():
	text.text = "When a living reaches this monument, they are safe and lives happily in the ever after"

func step_20():
	text.text = "But such a good thing must be balanced, that's why there are other beings. They live in a different dimension than the livings & us. "

func step_21():
	text.text = "Before Introducing you to the great vilain, lead Jimmy to the monument and offers him the happy ending."

func step_22():
	move_area()
	$npc.movement_allowed = true
	player.movement_allowed = true
	$layer/panel.hide()

func step_23():
	$layer/panel.show()
	if player.fsm.current_state == 'haunting':
		text.text = "Oh you brought Jimmy. Nah, we don't need him, sorry Jimmy I'll have to let you go."
		var jimmy = player.possess_target
		player.unpossess()
		jimmy.queue_free()
	else:
		text.text = "Here you are after saving a life !"
	player.movement_allowed = false
func step_24():
	text.text = "Okay now the scary stuff. Don't worry, you don't have to be affraid, they can't see you. But YOU can. Visit the other dimension now (press Tab)"

func step_25():
	$layer/panel.hide() # DIMENSION CHANGE

func step_26():
	$layer/panel.show()
	text.text = "This. This is a baaaad dude. Look at that teeth ! Brr."

func step_27():
	text.text = "We call them the \"deaths\", livings are funny thinking this is a one man job !"

func step_28():
	text.text = "Deaths wander in their own dimension. If they overlap with a living they feel it and try to bring them to their dimension, creating more deaths. You have very little time to save them by embodying them and running away !"

func step_29():
	text.text = "So remember, the death of a living is no big deal, but this will lead to even more deaths and we can't allow it as good guardian angels !"

func step_30():
	text.text = "Okay, I think we're done. Here is the recap of what you learned :"

func step_31():
	text.text = "\n- Living, angels & deaths live in three separated dimensions\n- Only the angels can navigate between dimension, but doing so they lose vision on the others (press Tab to switch)\n- You can possess and unpossess a living (by pressing space near them)\n- Livings will be safe at the monument\n- Deaths will replicate if they feed on the livings\n- Livings always appear at the same spots: their home, only visible to the livings\n"

func step_32():
	text.text = "Now you're ready to replace me. I'll remove this constraint so you can pass through walls. Have fun and don't mess there, if there are too many deaths we're screwed !"

func step_33():
	$layer/panel2.show()

func _on_beginning_body_entered(body):
	if body.is_in_group('players') or body.is_in_group('npcs'):
		if body.is_in_group('players'):
			player = body
		next_step()


func _on_cta_pressed():
	next_step()

func move_area():
	if current_area_position == null:
		current_area_position = 0
	else:
		current_area_position += 1

	var new_area_position = area_positions[current_area_position]
	$beginning.global_position = new_area_position.global_position

func on_dimension_changed(dimension):
	if (step != 25):
		return
	if (dimension != 'hell'):
		return
	next_step()
