extends Node2D

const DIMENSIONS = ['heaven', 'life', 'hell']

signal dimension_changed(dimension)

var current_dimension = 'heaven'


func _ready():
	set_dimension('heaven')
	pass

func set_dimension(dimension):
	assert(DIMENSIONS.find(dimension) != -1, 'Trying to set invalid dimension %s' % dimension)

	current_dimension = dimension
	emit_signal('dimension_changed', dimension)

func toggle_hell():
	if current_dimension == 'heaven':
		set_dimension('hell')
	elif current_dimension == 'hell':
		set_dimension('heaven')
