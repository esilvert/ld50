extends Node2D

class_name GWinLoseCondition

const win_screen = preload('res://scenes/win_screen/win_screen.tscn')
const lose_screen = preload('res://scenes/lose_screen/lose_screen.tscn')

const WIN_DURATION = 5 * 60

var nb_death_recorded = 0
var nb_souls_saved_recorded = 0

var elapsed_time = 0

func _ready():
	set_process(false)

func start_game():
	set_process(true)
	elapsed_time = 0
	nb_death_recorded = 0
	nb_souls_saved_recorded = 0

func _process(delta):
	elapsed_time += delta

	if elapsed_time > WIN_DURATION:
		print("GG t'as win")
		g_scene_manager.change_scene_to(win_screen)
		set_process(false)

func register_npc_death():
	nb_death_recorded += 1

func register_npc_saved():
	nb_souls_saved_recorded += 1
