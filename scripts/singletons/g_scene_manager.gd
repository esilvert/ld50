extends Node2D

var current_scene = null

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func change_scene_to(packed_scene):
	current_scene.queue_free()

	var scene = packed_scene.instance()
	get_tree().get_root().add_child(scene)
	get_tree().current_scene = scene
	current_scene = scene
