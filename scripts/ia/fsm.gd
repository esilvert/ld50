extends Node2D
class_name FSM

export(String) var INITIAL_STATE = 'SPAWN'
export(NodePath) var OWNER = null

signal state_changed(previous, new)

var STATES = ['spawn']

var previous_state = null
var current_state = null
var next_state = null

var state_duration = 0

func _ready():
	initialize_fsm()
	set_physics_process(true)

func initialize_fsm():
	set_next_state(INITIAL_STATE)

func _physics_process(delta):
	if(next_state):
		update_state_machine()

	if(owner.has_method('before_action')):
		owner.before_action(delta)

	process_current_state(delta)

	if(owner.has_method('after_action')):
		owner.after_action(delta)

	state_duration += delta

func update_state_machine():
	if(!next_state):
		return

	previous_state = current_state
	current_state = next_state
	next_state = null
	state_duration = 0

	print('FSM %s transitioned from %s to %s' % [owner.name, previous_state, current_state])


	var on_transition_state_function = ("on_exit_%s" % previous_state).to_lower()
	if(owner.has_method(on_transition_state_function)):
		owner.call(on_transition_state_function)

	on_transition_state_function = ("on_enter_%s" % current_state).to_lower()
	if(owner.has_method(on_transition_state_function)):
		owner.call(on_transition_state_function)

	emit_signal("state_changed", previous_state, current_state)

func process_current_state(delta):
	if(!current_state):
		push_error("FSM %s must have a current_state" % owner.name)
		return

	var state_functor = ("state_%s" % current_state).to_lower()

	if(owner.has_method(state_functor)):
		owner.call(state_functor, delta)

func set_next_state(state):
	next_state = state
