extends TextureRect
class_name OneDimensionOnly

export(String, 'hell', 'heaven', 'life') var VISIBLE_DIMENSION = 'life'

# Called when the node enters the scene tree for the first time.
func _ready():
	g_dimension.connect('dimension_changed', self, 'on_dimension_changed')
	on_dimension_changed(g_dimension.current_dimension)

func _exit_tree():
	g_dimension.disconnect('dimension_changed', self, 'on_dimension_changed')

func on_dimension_changed(dimension):
	if dimension == VISIBLE_DIMENSION:
		show()
	else:
		hide()
