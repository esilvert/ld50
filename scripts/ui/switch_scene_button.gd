extends Button

export(PackedScene) var TARGET_SCENE = null

func _pressed():
	g_scene_manager.change_scene_to(TARGET_SCENE)
