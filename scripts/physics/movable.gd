extends KinematicBody2D
class_name Movable

export(float) var SPEED = 160

var velocity : Vector2 = Vector2(0, 0)

var movement_allowed :bool = true

func move_with_velocity(normalized = true):
	if(!movement_allowed):
		return

	if normalized:
		velocity = move_and_slide(velocity.normalized() * SPEED)
	else:
		if velocity.length() > SPEED:
			velocity = velocity.normalized() * SPEED
	move_and_slide(velocity)
