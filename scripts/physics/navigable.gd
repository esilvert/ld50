extends Movable
class_name Navigable

const DISTANCE_THRESHOLD = 50

var navigation : Navigation2D = null
var path : Array = []
var path_point_duration = 0

var change_path_after = 999 #s

func _ready():
	var navigations = get_tree().get_nodes_in_group('navigation')
	assert(navigations.size() > 0, 'NPC could not find any navigation in group "navigation"')

	navigation = navigations[0]

func move_to_next_point(delta):
	var next_point = path[0]
	var distance = next_point - global_position

	var last_collision =  get_last_slide_collision()
	if (last_collision):
		var avoidance_vector = get_last_slide_collision().position - global_position
		velocity = -avoidance_vector
	else:
		velocity += distance * delta

	path_point_duration += delta

	if(distance.length() < DISTANCE_THRESHOLD):
		path.pop_front()
		path_point_duration = 0

	if (path_point_duration > change_path_after):
		pick_new_path()


func move_to_next_point_without_collision(delta):
	var next_point = path[0]
	var distance = next_point - global_position

	velocity = distance

	path_point_duration += delta

	if(distance.length() < DISTANCE_THRESHOLD):
		path.pop_front()
		path_point_duration = 0

	if (path_point_duration > change_path_after):
		pick_new_path()


func move_with_velocity(normalized = false):
	if(!movement_allowed):
		return

	move_and_slide(velocity.normalized() * SPEED)

func pick_new_path():
	path = navigation.get_simple_path_to_point_of_interest(self.global_position)
